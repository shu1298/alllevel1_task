package in.serosoft.academia.task7.collectionBasic;

import java.util.*;



public class ArraListPrintex {
	
	public static void main(String[]args)
	{
		
		  
		StudentData s1=new StudentData(1001,"shubham",60);
		StudentData s2=new StudentData(1002,"Rahul",49);
		StudentData s3=new StudentData(1003,"suresh",50);
		StudentData s4=new StudentData(1004,"ramesh",70);
		StudentData s5=new StudentData(1005,"satyam",78);
		StudentData s6=new StudentData(1006,"ratnesh",45);
	      
	      ArrayList<StudentData> st=new ArrayList<StudentData>();
	      ArrayList<StudentData> failed=new ArrayList<StudentData>();
	      st.add(s1);
	      st.add(s2);
	      st.add(s3);
	      st.add(s4);
	      st.add(s5);
	      st.add(s6);
	      
	      for(StudentData sd:st)
	      {
	    	  if(sd.getMarks()>60)
	    	  {
	    		  System.out.println("Got First Division"+sd);
	    	  }
	      }
	      
	      for(StudentData sd:st)
	      {
	    	  if(sd.getMarks()<50)
	    	  {
	    		  failed.add(sd);
	    	  }
	      }
	      System.out.println("GOT Failed"+failed);
	      
	      
	}
	}


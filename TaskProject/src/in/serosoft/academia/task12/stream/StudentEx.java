package in.serosoft.academia.task12.stream;

public class StudentEx {
	private String rno;
	private String name;
	private String branch;
	private int sem;
	private double  percentage;
	public String getRno() {
		return rno;
	}
	public void setRno(String rno) {
		this.rno = rno;
	}
	
	@Override
	public String toString() {
		return "StudentEx [rno=" + rno + ", name=" + name + ", branch=" + branch + ", sem=" + sem + ", percentage="
				+ percentage + "]";
	}
	public StudentEx(String rno, String name, String branch, int sem, double percentage) {
		super();
		this.rno = rno;
		this.name = name;
		this.branch = branch;
		this.sem = sem;
		this.percentage = percentage;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getBranch() {
		return branch;
	}
	public void setBranch(String branch) {
		this.branch = branch;
	}
	public int getSem() {
		return sem;
	}
	public void setSem(int sem) {
		this.sem = sem;
	}
	public double getPercentage() {
		return percentage;
	}
	public void setPercentage(double percentage) {
		this.percentage = percentage;
	}

}

package in.serosoft.academia.task12.stream;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ListOfStudent {
	public static void main(String[]args)
	{
		StudentEx student1 =new StudentEx("1001","Shubham1","CSE",1,87.1);
		StudentEx student2 =new StudentEx("1001","Shubham2","IT",2,88.1);
		StudentEx student3 =new StudentEx("1001","Shubham3","CSE",3,86.1);
		StudentEx student4 =new StudentEx("1001","Shubham4","IT",4,83.1);
		
		List<StudentEx> li=new ArrayList<StudentEx>();
		li.add(student1);
		li.add(student2);
		li.add(student3);
		li.add(student4);
		
		System.out.println("___________________________Orignal List___________________________________________________________");
		
		li.stream().forEach((stu)->System.out.println(stu));
		System.out.println("_____________________Task 1(Promot in Next Sem)____________________________________________________");
		
		List<StudentEx>finallist1=li.stream().map((s1)->{
			
			//StudentEx ste= new StudentEx(see1.getRno(),see1.getName(),see1.getBranch(),see1.getSem()+1,see1.getPercentage());
			s1.setSem(s1.getSem()+1);
			return s1;}).collect(Collectors.toList());
		
		for(StudentEx stte:finallist1)
		{
			System.out.println(stte);
		}
		//System.out.println("===========================================================================");
		//System.out.println(li);
		
		System.out.println("_______________________Task 2 (ALL CSE Student)_______________________________");
		
		List<StudentEx>finallist2=li.stream().filter((s2)->s2.getBranch().equals("CSE")).collect(Collectors.toList());
	
		for(StudentEx stte:finallist2)
		{
			System.out.println(stte);
		}
		System.out.println("_____________________Task 3 (Sorted order of percentage)___________________________________");
		
		
		Stream<StudentEx> sorted = li.stream().sorted((s1,s2)->(int)s1.getPercentage()-(int)s2.getPercentage());
		sorted.forEach((s1)->System.out.println(s1));
		
		
		System.out.println("_____________________Task 4 (Total of Pecentage)___________________________________");
		
		System.out.println("Not Done till now");
		
		//li.stream().reduce((stu1,stu2)->stu1.getPercentage()+stu2.getPercentage());
		
		System.out.println("_____________________Task 5 ALL (IT Transffered to CSE)___________________________________");
		li.stream().map((stu)->{
			String branch=stu.getBranch();
			if (branch.equals("IT"))
			{
				stu.setBranch("CSE");
			}
			return stu;}).forEach(System.out::println);
	}
	
	
	
	

}

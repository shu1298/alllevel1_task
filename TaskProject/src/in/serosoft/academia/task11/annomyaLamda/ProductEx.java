package in.serosoft.academia.task11.annomyaLamda;

public class ProductEx {
	
	private int productNo;
	private String productName;
	public int getProductNo() {
		return productNo;
	}
	public void setProductNo(int productNo) {
		this.productNo = productNo;
	}
	public String getProductName() {
		return productName;
	}
	public void setProductName(String productName) {
		this.productName = productName;
	}
	public ProductEx(int productNo, String productName) {
		super();
		this.productNo = productNo;
		this.productName = productName;
	}
	@Override
	public String toString() {
		return "ProductEx [productNo=" + productNo + ", productName=" + productName + "]";
	}
	
	

}

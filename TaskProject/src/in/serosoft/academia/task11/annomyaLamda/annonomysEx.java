package in.serosoft.academia.task11.annomyaLamda;

import java.util.*;

public class annonomysEx {

	public static void main(String[] args) {
		
		ProductEx product1=new ProductEx(1001,"Mouse");
		ProductEx product2=new ProductEx(1002,"KeyBoard");
		ProductEx product3=new ProductEx(1003,"Printer");
		
		List productList=new ArrayList<>();
		
		productList.add(product1);
		productList.add(product2);
		productList.add(product3);
		
		Comparator<ProductEx> comp=new Comparator<ProductEx>()
				{

					@Override
					public int compare(ProductEx p1, ProductEx p2) {
						
						return p1.getProductName().length() - p2.getProductName().length();
						
					}
			
				};
				//productList.sort(comp);
				
				Collections.sort(productList, comp);
				
				productList.forEach((pro)->{System.out.println(pro);});
				
				
	}

}

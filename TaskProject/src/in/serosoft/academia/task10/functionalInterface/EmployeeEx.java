package in.serosoft.academia.task10.functionalInterface;

public class EmployeeEx {
	
	private int eno;
	private String name;
	private int salary;
	private String desg;
	public int getEno() {
		return eno;
	}
	public void setEno(int eno) {
		this.eno = eno;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public int getSalary() {
		return salary;
	}
	public void setSalary(int salary) {
		this.salary = salary;
	}
	public String getDesg() {
		return desg;
	}
	public void setDesg(String desg) {
		this.desg = desg;
	}
	public EmployeeEx(int eno, String name, int salary, String desg) {
		super();
		this.eno = eno;
		this.name = name;
		this.salary = salary;
		this.desg = desg;
	}
	@Override
	public String toString() {
		return "EmployeeEx [eno=" + eno + ", name=" + name + ", salary=" + salary + ", desg=" + desg + "]";
	}

	
}

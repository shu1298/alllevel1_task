package in.serosoft.academia.task10.functionalInterface;

import java.util.*;

public class FunctionalIntEx {
	public static void main(String[]args)
	{
		
		EmployeeEx employee1= new EmployeeEx(1001, "Rahul1", 20000,"Eng");
		EmployeeEx employee2= new EmployeeEx(1001, "Rahul2", 30000,"Analyst");
		EmployeeEx employee3= new EmployeeEx(1001, "Rahul3", 40000,"Manager");
		EmployeeEx employee4= new EmployeeEx(1001, "Rahul4", 50000,"HR");
		EmployeeEx employee5= new EmployeeEx(1001, "Rahul5", 60000,"Executive");
	
    List<EmployeeEx> employeeList=new ArrayList<>();
    employeeList.add(employee1);
    employeeList.add(employee2);
    employeeList.add(employee3);
    employeeList.add(employee4);
    employeeList.add(employee5);
    
    employeeList.forEach((emp)->{
    	if (emp.getDesg().equals("Analyst"))
    	{
    		emp.setSalary(emp.getSalary()+emp.getSalary()*10/100);
    		//System.out.println(emp);
    	}
    	else if(emp.getDesg().equals("Executive"))
    	{
    		emp.setSalary(emp.getSalary()+emp.getSalary()*5/100);
    	}
    });
    
    employeeList.forEach((emp)->System.out.println(emp));
    
	}
}
